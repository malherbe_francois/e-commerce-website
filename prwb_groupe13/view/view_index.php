<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Bienvenue sur Projet13</title>
        <base href="<?= $web_root ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style1.css">
    </head>
    <body id="profil" >
        <div class="title">Bienvenue sur Projet13</div>
        <div class="menu">
            <a href="login/login">Connection</a>
            <a href="login/signup">S'enregistrer</a>
        </div>
        <div class="main">
            Enregistrez ou connectez-vous.
        </div>
    </body>
</html>
