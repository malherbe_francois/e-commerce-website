<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Error</title>
        <base href="<?= $web_root ?>"/>
        <link rel="stylesheet" type="text/css" href="css/style1.css">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <div class="title">Error</div>
        <div class="main">
            <?= $error ?>
        </div>
    </body>
</html>
